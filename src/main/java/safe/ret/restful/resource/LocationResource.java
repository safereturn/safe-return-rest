package safe.ret.restful.resource;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import safe.ret.hibernate.model.Location;
import safe.ret.hibernate.model.TrackedUser;
import safe.ret.hibernate.service.LocationService;
import safe.ret.restful.dto.LocationDto;
import safe.ret.restful.dto.SafeReturnMessage;
import safe.ret.restful.service.CheckLocationService;
import safe.ret.restful.utils.Params;

@Path("/location")
public class LocationResource {

	private LocationService locationService = new LocationService();
	private CheckLocationService checkLocationService = new CheckLocationService();
	 
	@POST
 	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveLocation(LocationDto locationDto) {
 
		//TODO handle constraint violation exception fk_user does not exist.
		Location location = locationService.createLocation(this.getLocation(locationDto));
		return Response.ok(new LocationDto(location)).build();
	}
 
	
	@GET
 	@Produces(MediaType.APPLICATION_JSON)
	public Response getLocationsOfTrackedUser(@QueryParam(Params.TRACKED_USER_ID_PARAM) Long id,@QueryParam(Params.SAFE_LOCATION) Boolean isSafe) {

 		List<Location> list = null;
 		if (isSafe==null)
			list = locationService.getAllLocationsByTrackedUser(id);
		else
			list = locationService.getLocationsOfTrackedUser(id,isSafe);
 
 		List<LocationDto> dtos = new ArrayList<LocationDto>(list.size());
 		list.forEach(item->dtos.add(new LocationDto(item)));
		return Response.ok(dtos).build();
	}
 
	
	@GET
	@Path("/last-location")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLastKnownLocation(@QueryParam(Params.TRACKED_USER_ID_PARAM) Long id) {
		
 		Location location = locationService.getLastKnownLocation(id);
		LocationDto dto = new LocationDto(location);
		return Response.ok().entity(dto).build();
 
	}
 
	/**
	 * 
	 * Receives current location of tracked user,stores it and
	 * checks if it's inside the safe range.If tracked user is moved from 
	 * a safe location to an unsafe or the opposite,notifies contact persons.
	 *  
	 * @param locationJSON
	 * @return
	 */
	 
	@POST
	@Path("/check-location")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkLocation(LocationDto locationDto) {
		
		boolean isSafe = true;
				
		System.out.println("Asking for safety");; 
 		 
 		//store current location
 		Location location = locationService.createLocation(this.getLocation(locationDto));
 		
		List<Location> safeLocations = locationService.getLocationsOfTrackedUser(location.getTrackedUser().getId(),true);
		if (safeLocations.isEmpty()){
			return Response.ok().entity(new SafeReturnMessage("Safe locations are not specified")).build();
		}
		
 		isSafe = checkLocationService.checkLocation(location,safeLocations);
 		
 		
 		String outputMessage = isSafe?"Folk is safe":"Folk is lost";
 		return Response.ok().entity(new SafeReturnMessage(outputMessage)).build();
 		
	}
	
	//TODO VALIDATE
	private Location getLocation(LocationDto locationDto){
		
		Location location = new Location();
		location.setDate(locationDto.getDate());
		location.setHome(locationDto.isHome());
		TrackedUser trackedUser = new TrackedUser();
		trackedUser.setId(locationDto.getTrackedUserId());
		location.setTrackedUser(trackedUser);
		location.setLatitude(locationDto.getLatitude());
		location.setLongitude(locationDto.getLongitude());
		return location;
	}
}
