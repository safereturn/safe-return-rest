package safe.ret.restful.resource;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import safe.ret.hibernate.model.ContactPerson;
import safe.ret.hibernate.model.TrackedContactAssoc;
import safe.ret.hibernate.model.TrackedUser;
import safe.ret.hibernate.service.ContactPersonService;
import safe.ret.hibernate.service.TrackedUserService;
import safe.ret.hibernate.utils.Globals;
import safe.ret.restful.dto.ContactDto;
import safe.ret.restful.dto.SafeReturnMessage;
import safe.ret.restful.dto.UserDto;
import safe.ret.restful.service.messaging.MessageTemplate;
import safe.ret.restful.service.messaging.MessagingService;
import safe.ret.restful.utils.Params;

@Path("/tracked-user")
public class TrackedUserResource {

	private TrackedUserService trackedUserService = new TrackedUserService();
	private ContactPersonService contactPersonService= new ContactPersonService();
	private MessagingService messagingService = new MessagingService();

	
	
	@PUT
 	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createTrackedUser(UserDto userDto){
		
		TrackedUser trackedUser = trackedUserService.saveOrUpdateTrackedUser(this.getTrackedUser(userDto));
		return Response.ok().entity(new UserDto(trackedUser)).build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTrackedUser(@QueryParam("id") Long id) {
		
		TrackedUser trackedUser = trackedUserService.getTrackedUserByID(id);
		return Response.ok().entity(new UserDto(trackedUser)).build();
	}
	
	@DELETE
 	@Produces(MediaType.APPLICATION_JSON)
	public String deleteTrackedUser(@QueryParam("id") Long id) {		
		
		if(trackedUserService.deleteTrackedUser(id))
			return Globals.SUCCESS;
		else
			return Globals.FAILURE;
	}
	
	
	@POST
	@Path("/contact")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addContactPerson(ContactDto contactDTO) {
 	
		// check if contact person's mobile is registered
		ContactPerson person = contactPersonService.findByMobile(contactDTO.getMobile());

		if (person == null){ // contact person is not registered
	
			return Response.status(Status.BAD_REQUEST).entity(new SafeReturnMessage("Not registered - Unknown mobile number")).build();
		} else {
		
			//TODO handle when received id is null or tracked person does not exist
			TrackedUser tu = trackedUserService.loadTrackedUserById(contactDTO.getTrackedUserId());
			
			if (trackedUserService.hasContact(tu,person.getId())){
				
 				return Response.status(Status.BAD_REQUEST).entity(new SafeReturnMessage("Already in contacts")).build();
 			
			}else{
		
				tu.getTrackedContactAssoc().add(new TrackedContactAssoc(tu, person, contactDTO.getDisplayName()));
				tu = trackedUserService.saveOrUpdateTrackedUser(tu);
	 			MessageTemplate msg =  messagingService.createContactAddedMessage(tu);
	 			messagingService.notifyOne(msg,person.getRegistrationToken());
				return Response.ok().entity(new SafeReturnMessage("Contact added")).build();
 			}
		}
	}


//	@GET
//	@Path("/get-contacts")
// 	@Produces(MediaType.APPLICATION_JSON)
//	public Response getContacts(@QueryParam(Params.TRACKED_USER_ID_PARAM) Long trackedId) {
// 		
//		TrackedUser u = trackedUserService.loadTrackedUserById(trackedId);
// 		List<ContactDto> list = new ArrayList<ContactDto>();
//		u.getTrackedContactAssoc().forEach(assoc->list.add(new ContactDto(assoc.getContactPerson().getMobile(),assoc.getContactDisplayName())));
//		return Response.ok().entity(list).build();
//	}
	 
	
	
	private TrackedUser getTrackedUser(UserDto userDto){
		
		TrackedUser cp = new TrackedUser();
		cp.setId(userDto.getId());
		cp.setMobile(userDto.getMobile());
		cp.setRegistrationToken(userDto.getRegistrationToken());
		cp.setUsername(userDto.getUsername());
		cp.setPassword(userDto.getPassword());
		cp.setRegistrationDate(userDto.getRegistrationDate());
		return cp;
	}
	
}
