package safe.ret.restful.dto;

import safe.ret.hibernate.model.Options;

public class OptionDto {

	private double distance;
	private int time;
	private long trackedUserId;
	
	public OptionDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OptionDto(Options options){
		
		this.distance = options.getAllowedDistance();
		this.time = options.getTimeInterval();
		this.trackedUserId = options.getTrackedUser().getId();
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public long getTrackedUserId() {
		return trackedUserId;
	}

	public void setTrackedUserId(long trackedUserId) {
		this.trackedUserId = trackedUserId;
	}
	
	
	
}
