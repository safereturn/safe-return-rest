package safe.ret.restful.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import safe.ret.hibernate.model.Options;
import safe.ret.hibernate.model.TrackedUser;
import safe.ret.hibernate.service.OptionsService;
import safe.ret.restful.dto.OptionDto;
import safe.ret.restful.utils.Params;

@Path("/options")
public class OptionsResource {
	
	private OptionsService optionsService = new OptionsService();

	@GET
 	@Produces(MediaType.APPLICATION_JSON)
	public Response getOptionsOfTrackedUser(@QueryParam(Params.TRACKED_USER_ID_PARAM) Long id) {
		
 		Options opt = optionsService.getOptionsByUserId(id);
 		return Response.ok().entity(new OptionDto(opt)).build();
	}
	

	@PUT
 	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response setOptions(OptionDto optionsDto) {
  		Options stored = optionsService.getOptionsByUserId(optionsDto.getTrackedUserId());
		Options opt = optionsService.saveOrUpdateOptions(this.getOptions(optionsDto,stored==null?null:stored.getId()));
		return Response.ok().entity(new OptionDto(opt)).build();
	}

	
	private Options getOptions(OptionDto optionsDto,Long id) {
		 
		Options options = new Options();
		options.setId(id);
		TrackedUser tu = new TrackedUser();
		tu.setId(optionsDto.getTrackedUserId());
		options.setTrackedUser(tu);
		options.setAllowedDistance(optionsDto.getDistance());
		options.setTimeInterval(optionsDto.getTime());
		return options;
	}
	
}
