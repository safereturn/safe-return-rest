package safe.ret.restful.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import safe.ret.hibernate.model.ContactPerson;
import safe.ret.hibernate.model.TrackedUser;
import safe.ret.hibernate.service.ContactPersonService;
import safe.ret.hibernate.service.TrackedUserService;
import safe.ret.restful.dto.UpdateTokenRequest;
 
@Path("/user")
public class SaferetUserResource {

	public static final String CONTACT = "CONTACT";
	public static final String TRACKED = "TRACKED";
	
	private TrackedUserService trackedUserService = new TrackedUserService();
	private ContactPersonService contactPersonService = new ContactPersonService();
	
	
	/**
	 * Sample input
	 * {
	 * 		user : "12",
	 * 		token  : "de55Gc9N-9s:APA91bF6zI6py4YTzt0Z1Iaezo6k7hefKEgLaytVkpei1_anIp2Rxr-HbgarFS5_m3EWJ4yFj-4GtreG4crmwUQKT0C5haMEnFh3NrbVQoAnUOpj8Lo-SJp53c90bkA4sBG-Wjs9fxCA"
	 * 		userType : CONTACT/TRACKED	
	 * }
	 * 
 	 *  Must be called only after user is registered already
	 */
	@POST
	@Path("/updateToken")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public boolean storeRegistrationToken(UpdateTokenRequest updateTokenRequest){
		boolean success = false;
		
		Long userId = Long.valueOf(updateTokenRequest.getUserId());
		String userType = updateTokenRequest.getUserType();
		String token = updateTokenRequest.getRefreshedToken();
 
		if(userType.equals(CONTACT)){
			updateContactPerson(userId, token );
			success = true;
		}else if(userType.equals(TRACKED)){
			updateTrackedPerson(userId,token );
			success = true;
		}
		if(success)	
			System.out.println("Updated registration token "+ updateTokenRequest.getUserType() 
				+"\n"+ updateTokenRequest.getRefreshedToken());;
	
		return success;	
	}
	
	
	private TrackedUser updateTrackedPerson(Long userId, String token ) {
		TrackedUser u = trackedUserService.getTrackedUserByID(userId);
		u.setRegistrationToken(token);
		trackedUserService.updateToken(userId,token);
 		return u;		
	}
	
	private ContactPerson updateContactPerson(Long userId, String token ) {
		ContactPerson p = contactPersonService.getContactPersonByID(userId);
		p.setRegistrationToken(token);
		contactPersonService.updateToken(userId,token);
 		return p;
	}
	
}
