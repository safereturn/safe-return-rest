package safe.ret.restful.service.messaging;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import safe.ret.hibernate.model.Location;
import safe.ret.hibernate.model.TrackedUser;

public class MessagingService {

	private static final Logger logger = LoggerFactory.getLogger(MessagingService.class);

	private static String FCM_ENDPOINT = "https://fcm.googleapis.com/fcm/send";

	private static String API_KEY = null;

	static {

		initialize();
	}

	public static void main(String[] args) {
	
		MessagingService msgService = new MessagingService();
		MessageTemplate temp = new MessageTemplate();
		temp.setTitle("Contact added");
		temp.setCode("LOC_ALERT");
		temp.getBody().put("msg", "My cool message");
		msgService.notifyOne(temp, "e2Sm5EGc-SA:APA91bFXbS4odDmP_JfgjPXEbLmhbAeAWGFMPRE-K7foGvwbX8swvEeQpSo4nhlRpTt21JfjMD6TT5-xg1MejRafwVkmNqw2UsAF1NMfD-dIgxZ4T_HQGG2tGK8iTA9IGw69CVdoZ4F9");
	}

	public boolean notifyOne(MessageTemplate messageTemplate , String regID) {
		
		FCMMessage fcmMessage = prepareFCMMessage(messageTemplate, regID);
		String jsonData = fcmMessage.formatAsJSONString();
		if (jsonData==null)
			return false;
		else 
			return  performCall(jsonData);
		
	}
	
	public boolean notifyAll(MessageTemplate messageTemplate , List<String> regIDs) {
		
		FCMMessage fcmMessage = prepareFCMMessage(messageTemplate, regIDs.toArray(new String[regIDs.size()]));
		String jsonData = fcmMessage.formatAsJSONString();
		if (jsonData==null)
			return false;
		else 
			return  performCall(jsonData);
	}
	
	 
	
	
	private FCMMessage prepareFCMMessage(MessageTemplate messageTemplate,String ...registrationIds){

		FCMMessage fcmMessage = new FCMMessage();
		fcmMessage.setRegistration_ids(registrationIds);
		fcmMessage.setData(messageTemplate);
		fcmMessage.setIcon("");
		fcmMessage.setPriority("high");
		return fcmMessage;
	}
	
	private boolean performCall (String jsonData){
	
			try {
				logger.debug(jsonData);
				String response = 	Request.Post(FCM_ENDPOINT)
						.addHeader("Authorization", API_KEY)
						.addHeader("Content-type", "application/json")
			 			.bodyString(jsonData , ContentType.APPLICATION_JSON)
						.execute().returnContent().asString()  ;
				
				logger.debug(response);
				return true;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
	}
 
	public MessageTemplate createContactAddedMessage(TrackedUser tu) {
 
		MessageTemplate temp = new MessageTemplate();
		temp.setTitle("Contact added");
		temp.setCode("CTAD");
		temp.getBody().put("tracked_user_id", tu.getId());
		temp.getBody().put("tracked_user_name", tu.getUsername());
 
		return temp;
	}

	public MessageTemplate createLocationInfoMessage(TrackedUser tu, Location loc){ 
 
		MessageTemplate temp = new MessageTemplate();
		temp.setTitle("Safe return");
		temp.setCode("LOCINF");
		temp.getBody().put("lang",loc.getLatitude());
		temp.getBody().put("lat", loc.getLongitude());
		String status = tu.isWithinRadius() ? "back to safety" : "lost";
		temp.getBody().put("msg", tu.getUsername()+ " is "+status);
		return temp;
 
	}

	private static void initialize() {

		Properties prop = new Properties();

		String propFileName = "config.properties";

		InputStream inputStream = MessagingService.class.getClassLoader().getResourceAsStream(propFileName);

		if (inputStream != null) {
			try {
				prop.load(inputStream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			System.out.println("property file '" + propFileName + "' not found in the classpath");
		}

		API_KEY = prop.getProperty("FCM_API_KEY");

		logger.debug("Restored FCM API KEY");
	}

}
