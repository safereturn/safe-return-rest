package safe.ret.restful.dto;

public class SafeReturnMessage {
 
	private String message;
	
	public SafeReturnMessage(String message){
 		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}