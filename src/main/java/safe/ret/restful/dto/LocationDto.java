package safe.ret.restful.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import safe.ret.hibernate.model.Location;

public class LocationDto {

	private double latitude;
	private double longitude;
	private long trackedUserId;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date date;
	private boolean home;
	private String trackedUserName;
	
	
	
	public LocationDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LocationDto(Location location){
	
		this.latitude = location.getLatitude();
		this.longitude = location.getLongitude();
		this.trackedUserId = location.getTrackedUser().getId();
		this.date = location.getDate();
		this.home = location.isHome();
		this.trackedUserName = location.getTrackedUser().getUsername();
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public long getTrackedUserId() {
		return trackedUserId;
	}

	public void setTrackedUserId(long trackedUserId) {
		this.trackedUserId = trackedUserId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isHome() {
		return home;
	}

	public void setHome(boolean home) {
		this.home = home;
	}

	public String getTrackedUserName() {
		return trackedUserName;
	}

	public void setTrackedUserName(String trackedUserName) {
		this.trackedUserName = trackedUserName;
	}
	
	
}
