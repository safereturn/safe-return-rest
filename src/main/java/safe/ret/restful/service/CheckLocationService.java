package safe.ret.restful.service;

import java.util.ArrayList;
import java.util.List;

import safe.ret.hibernate.model.Location;
import safe.ret.hibernate.model.Options;
import safe.ret.hibernate.model.TrackedUser;
import safe.ret.hibernate.service.OptionsService;
import safe.ret.hibernate.service.TrackedUserService;
import safe.ret.hibernate.utils.Globals;
import safe.ret.hibernate.utils.Utils;
import safe.ret.restful.service.messaging.MessageTemplate;
import safe.ret.restful.service.messaging.MessagingService;

public class CheckLocationService  {

	private TrackedUserService trackedUserService = new TrackedUserService();
	private MessagingService messagingService = new MessagingService();
 	private OptionsService optionsService = new OptionsService();
 
 	//TODO break down method
 	//1.check location 
 	//2.notify contacts if any
	public boolean checkLocation(Location location,List<Location> safeLocations) {

		boolean isSafe;

		// check if safe
		isSafe = isCurrentLocationSafe(location,safeLocations);
		// fetch tracked user
		TrackedUser tu = trackedUserService.loadTrackedUserById(location.getTrackedUser().getId());

		if (tu.isWithinRadius() == isSafe) // state of tracked user has not changed
		{
			// don't do nothing
		} else {
			tu.setWithinRadius(isSafe);
			trackedUserService.saveOrUpdateTrackedUser(tu);
			List<String> regIds = new ArrayList<String>(); 
			MessageTemplate messageTemplate = messagingService.createLocationInfoMessage(tu, location);
			tu.getTrackedContactAssoc().forEach(
					item->regIds.add(item.getContactPerson().getRegistrationToken())
					);
			messagingService.notifyAll(messageTemplate, regIds );
 
		}
		
 
		return isSafe;

	}

	
	
	
	private boolean isCurrentLocationSafe(Location location,List<Location> safeLocations) {
		System.out.println(location.getLongitude()+ "  " +location.getLatitude());
		double safeDistance = getSafeDistance(location.getTrackedUser().getId());
 		for (Location safe : safeLocations) {
			// in km
 			System.out.println(safe.getLongitude()+ "  " +safe.getLatitude());
			double distanceFromSafety = Utils.distFrom(location.getLatitude(), location.getLongitude(), safe.getLatitude(), safe.getLongitude());
			System.out.println("Distance from safe point is : " + distanceFromSafety + " km or "
					+ distanceFromSafety * 1000 + " meters");
			if (distanceFromSafety  < safeDistance) {
				return true;
			}
		}
		return false;
	}

	// Default safe distance from a specified location in meters,used when no value is specified by user.
	public static double DEFAULT_SAFE_DISTANCE_KM = 1;  
	// in km
	private double getSafeDistance(Long id) {

		Options options = optionsService.getOptionsByUserId(id);
		if (options != null) {
			System.out.println("Retrived option:" + options.getAllowedDistance() + " km");
			return options.getAllowedDistance();
		} else {
			System.out.println("Returning default:" + DEFAULT_SAFE_DISTANCE_KM + " km");

			return DEFAULT_SAFE_DISTANCE_KM;
		}

	}

}
