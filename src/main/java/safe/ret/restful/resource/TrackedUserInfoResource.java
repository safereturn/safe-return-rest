//package safe.ret.restful.resource;
//
//import javax.ws.rs.Consumes;
//import javax.ws.rs.GET;
//import javax.ws.rs.PUT;
//import javax.ws.rs.Path;
//import javax.ws.rs.Produces;
//import javax.ws.rs.QueryParam;
//import javax.ws.rs.core.MediaType;
//
//import com.google.gson.Gson;
//
//import safe.ret.hibernate.model.TrackedUserInfo;
//import safe.ret.hibernate.service.TrackedUserInfoService;
//import safe.ret.hibernate.utils.Globals;
//
//@Path("/tracked-user-info-service")
//public class TrackedUserInfoResource {
//
//	private TrackedUserInfoService trackedUserInfoService = new TrackedUserInfoService();
//	
//	@GET
//	@Path("/tracked-user-info")
//	@Produces(MediaType.APPLICATION_JSON)
//	public String getTrackedUserInfo(@QueryParam("trackedUserID") Long id) {
//		
//		String response = Globals.EMPTY_STRING;
//		TrackedUserInfo userInfo = trackedUserInfoService.getTrackedUserInfoByTrackedUser(id);
//		Gson gson = new Gson();
//		response = gson.toJson(userInfo);
//		
//		return response;
//	}
//	
//	@PUT
//	@Path("/tracked-user-info")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.TEXT_PLAIN)
//	public String saveOrUpdateTrackedUserInfo(String trackedUserInfoJSON) {
//		
//		Gson gson = new Gson();
//		TrackedUserInfo userInfo = gson.fromJson(trackedUserInfoJSON, TrackedUserInfo.class);
//		if(trackedUserInfoService.saveOrUpdateTrackedUserInfo(userInfo))
//			return Globals.SUCCESS;
//		else
//			return Globals.FAILURE;
//		
//	}
//	
//}
