package safe.ret.restful.service.messaging;

import java.util.HashMap;
import java.util.Map;

public class MessageTemplate {
 
	private Map<String,Object> body = new HashMap<>();
	private String code;
	private String title;
 	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
 
	public Map<String, Object> getBody() {
		return body;
	}
	public void setBody(Map<String, Object> body) {
		this.body = body;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	
	
 
}
