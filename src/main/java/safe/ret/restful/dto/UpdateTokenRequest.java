package safe.ret.restful.dto;

public class UpdateTokenRequest {

	private String userId;
    private String userType;
    private String refreshedToken;

    public UpdateTokenRequest() {
    }
    
    public UpdateTokenRequest(String userId, String userType, String refreshedToken) {
        this.userId = userId;
        this.userType = userType;
        this.refreshedToken = refreshedToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getRefreshedToken() {
        return refreshedToken;
    }

    public void setRefreshedToken(String refreshedToken) {
        this.refreshedToken = refreshedToken;
    }
	    
}
