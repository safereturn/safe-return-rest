package safe.ret.restful.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import safe.ret.hibernate.model.ContactPerson;
import safe.ret.hibernate.model.SafeReturnUser;

public class UserDto {
	
	private Long id;
	private Long trackingId;
	private String mobile;
	private String name;
	private String surname;
	private String username;
	private String registrationToken;
	@JsonProperty (access = Access.WRITE_ONLY) //don't expose during deserialisation
	private String password;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ")
	private Date registrationDate;
	
	
	
	public UserDto() {
		super();
	}
	
	
	public UserDto(SafeReturnUser user){
		
		this.id = user.getId();
		this.mobile = user.getMobile();
		this.name = user.getName();
		this.surname = user.getSurname();
		this.username = user.getUsername();
		this.registrationToken = user.getRegistrationToken();
		this.password = user.getPassword();
		this.registrationDate = user.getRegistrationDate();
	}
	
	public UserDto(ContactPerson user){
		
		this.id = user.getId();
		this.trackingId = user.getTrackedUser().getId();
		this.mobile = user.getMobile();
		this.name = user.getName();
		this.surname = user.getSurname();
		this.username = user.getUsername();
		this.registrationToken = user.getRegistrationToken();
		this.password = user.getPassword();
		this.registrationDate = user.getRegistrationDate();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTrackingId() {
		return trackingId;
	}

	public void setTrackingId(Long trackingId) {
		this.trackingId = trackingId;
	}

	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

 	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRegistrationToken() {
		return registrationToken;
	}

	public void setRegistrationToken(String registrationToken) {
		this.registrationToken = registrationToken;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	 

}
