package safe.ret.restful.dto;

public class ContactDto {
	
	private String mobile;
	private String displayName;
	private Long trackedUserId;
	
	
	public ContactDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ContactDto(String mobile, String displayName) {
		super();
		this.mobile = mobile;
		this.displayName = displayName;
	}
 
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
 
	public String getDisplayName() {
		return displayName;
	}
	
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public Long getTrackedUserId() {
		return trackedUserId;
	}
	
	public void setTrackedUserId(Long trackedUserId) {
		this.trackedUserId = trackedUserId;
	}
	

}
