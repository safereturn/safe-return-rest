package safe.ret.restful.resource;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import safe.ret.hibernate.model.ContactPerson;
import safe.ret.hibernate.model.TrackedUser;
import safe.ret.hibernate.service.ContactPersonService;
import safe.ret.hibernate.service.TrackedUserService;
import safe.ret.hibernate.utils.Globals;
import safe.ret.restful.dto.ContactDto;
import safe.ret.restful.dto.UserDto;
import safe.ret.restful.utils.Params;

@Path("/contact-person")
public class ContactPersonResource {

	private ContactPersonService contactPersonService = new ContactPersonService();
	private TrackedUserService trackedUserService = new TrackedUserService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getContactPerson(@QueryParam(Params.ID_PARAM) Long id) {
		
		ContactPerson contactPerson = contactPersonService.getContactPersonByID(id);
		return Response.ok().entity(new UserDto(contactPerson)).build();
	}

	@GET
	@Path("/get-contacts")
 	@Produces(MediaType.APPLICATION_JSON)
	public Response getContacts(@QueryParam(Params.TRACKED_USER_ID_PARAM) Long trackedId) {
 		
		TrackedUser trackedUser = trackedUserService.loadTrackedUserById(trackedId);
		List<ContactPerson> contacts = contactPersonService.getContactsByTrackedUser(trackedUser);
 		List<ContactDto> list = new ArrayList<ContactDto>();
		contacts.forEach(contact->list.add(new ContactDto(contact.getMobile(),contact.getName()+" " + contact.getSurname())));
		return Response.ok().entity(list).build();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveOrUpdateContactPerson(UserDto userDto) {

 		ContactPerson contactPerson = contactPersonService.saveOrUpdateContactPerson(this.getContactPerson(userDto));
 		return Response.ok().entity(new UserDto(contactPerson)).build();
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public String deleteContactPerson(@QueryParam(Params.ID_PARAM) Long id) {

		if (contactPersonService.deleteContactPerson(id))
			return Globals.SUCCESS;
		else
			return Globals.FAILURE;

	}
	
	
	private ContactPerson getContactPerson(UserDto userDto){
		
		ContactPerson cp = null;
				
		TrackedUser trackedUser = trackedUserService.getTrackedUserByIdPass(userDto.getTrackingId(), userDto.getPassword());
		if(trackedUser!=null) {
			cp = new ContactPerson();
			cp.setId(userDto.getId());
			cp.setTrackedUser(trackedUser);
			cp.setMobile(userDto.getMobile());
//			cp.setRegistrationToken(userDto.getRegistrationToken());
			cp.setName(userDto.getName());
			cp.setSurname(userDto.getSurname());
//			cp.setUsername(userDto.getUsername());
			cp.setPassword(userDto.getPassword());
			cp.setRegistrationDate(userDto.getRegistrationDate());
		}

		return cp;
	}
}
